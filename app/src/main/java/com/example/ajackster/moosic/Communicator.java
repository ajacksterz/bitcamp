package com.example.ajackster.moosic;

import java.util.ArrayList;
public interface Communicator {
     void respond(int position, ArrayList<SongsDatas> songs);
}
