package com.example.ajackster.moosic;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class MoosicRoom extends AppCompatActivity implements View.OnClickListener {
    private static ArrayList<SongsDatas> mySongs,listOfSongs;
    private ListView songs,rooms;
    private static SongDataAdapter adapter, adapter2;
    private static final String url = "http://45.55.182.4:8080/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_moosic_room);
        rooms = (ListView) findViewById(R.id.listRooms);
        songs = (ListView) findViewById(R.id.listSongs);
        Button add = (Button) findViewById(R.id.btnAdd);
        Button update = (Button) findViewById(R.id.btnUpdate);
        Button leave = (Button) findViewById(R.id.btnLeave);
        leave.setOnClickListener(this);
        add.setOnClickListener(this);
        update.setOnClickListener(this);
        adapter = new SongDataAdapter(getApplicationContext(),
                R.layout.activity_song_data_adapter);
        adapter2 = new SongDataAdapter(getApplicationContext(),
                R.layout.activity_song_data_adapter);
        mySongs = new ArrayList<>();
        Thread t = new Thread() {
            @Override
            public void run() {
                super.run();
                getListOfSongs();
                updateList();
            }
        };
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        rooms.setAdapter(adapter);
        rooms.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, final int pos, long id) {
                playSong(mySongs.get(pos).getSongPath());
            }
        });
        rooms.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                SharedPreferences groupPref = getSharedPreferences("groupInfo", Context.MODE_PRIVATE);
                String groupID = groupPref.getString("groupID", "");
                delete(groupID, mySongs.get(position).getSongNames());
                return true;
            }
        });
        songs.setAdapter(adapter2);
        songs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int pos, long id) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MoosicRoom.this);
                builder.setCancelable(false);
                builder.setMessage("Do you want to add the song?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferences groupPref = getSharedPreferences("groupInfo", Context.MODE_PRIVATE);
                        final String groupID = groupPref.getString("groupID", "");
                        Thread t = new Thread() {
                            @Override
                            public void run() {
                                super.run();
                                upload(listOfSongs.get(pos).getSongNames(), listOfSongs.get(pos).getSongArtist(),
                                        listOfSongs.get(pos).getSongPath(), groupID);
                            }
                        };
                        t.start();
                        songs.setVisibility(View.GONE);
                        try {
                            t.join();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        songs.setVisibility(View.GONE);
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

            updateList();

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAdd:
                if (songs.isShown())
                    songs.setVisibility(View.GONE);
                else
                    songs.setVisibility(View.VISIBLE);
                break;
            case R.id.btnUpdate:
                updateList();
                break;
            case R.id.btnLeave:
                SharedPreferences groupPref = getSharedPreferences("groupInfo", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = groupPref.edit();
                editor.clear();
                editor.apply();
                startActivity(new Intent(MoosicRoom.this, Tabs.class));
        }
    }
    private void getListOfSongs(){
        listOfSongs = new ArrayList<>();
        try {
            URL obj = new URL(url +"getListOfSongs");
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            String userInfo = response.toString();
            JSONArray jArray = new JSONArray(userInfo);
            for (int i =0; i < jArray.length(); i++) {
                JSONObject songObject = jArray.getJSONObject(i);
                String songName = songObject.getString("songName");
                String artist = songObject.getString("artist");
                String fileName = songObject.getString("filename");
                SongsDatas songsDatas  = new SongsDatas(songName,artist,fileName,"");
                SongDataProvider provider = new SongDataProvider(null, songName, artist);
                adapter2.add(provider);
                listOfSongs.add(songsDatas);
            }
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void getFromDB(String groupID) {
        try {
            URL obj = new URL(url + "getGroupSongs/" + groupID);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            String userInfo = response.toString();
            JSONArray jArray = new JSONArray(userInfo);
            for (int i = mySongs.size(); i < jArray.length(); i++) {
                JSONObject songObject = jArray.getJSONObject(i);
                String songName = songObject.getString("title");
                String artist = songObject.getString("artist");
                String songUrl =songObject.getString("file");
                SongsDatas songsDatas  = new SongsDatas(songName,artist,songUrl,"");
                mySongs.add(songsDatas);
            }
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void delete(String groupID, String title) {
        try {
            URL obj = new URL(url + "deleteSong/" + groupID + '/' + title);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void upload(String title, String artist, String fileS, String groupID) {
        try {
            String urlParams = "chicken=ah" + "&title=" + title + "&artist=" + artist + "&file=" + fileS + "&groupID=" + groupID;
            URL obj = new URL(url + "uploadSong");
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeUTF(urlParams);
            System.out.println(title + artist + fileS + "-------");
            wr.flush();
            wr.close();
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void updateList() {
        Thread t = new Thread() {
            @Override
            public void run() {
                super.run();
                SharedPreferences groupPref = getSharedPreferences("groupInfo", Context.MODE_PRIVATE);
                String groupID = groupPref.getString("groupID", "");
                getFromDB(groupID);
            }
        };
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        adapter.clear();
        for (int i = 0; i < mySongs.size() ; i++) {
            SongDataProvider provider = new SongDataProvider(null, mySongs.get(i).getSongNames(),mySongs.get(i).getSongArtist());
            adapter.add(provider);
        }
        adapter.notifyDataSetChanged();
    }
    private void playSong(String fileName) {
        MediaPlayer mPlayer = new MediaPlayer();
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        String s = "http://45.55.182.4:8080/getSongFile/" + fileName;
        try {
            mPlayer.setDataSource(s);
        } catch (IllegalArgumentException e) {
            Toast.makeText(getApplicationContext(), "Server is down", Toast.LENGTH_LONG).show();
        } catch (SecurityException e) {
            Toast.makeText(getApplicationContext(), "Server is down", Toast.LENGTH_LONG).show();
        } catch (IllegalStateException e) {
            Toast.makeText(getApplicationContext(), "Server is down", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            mPlayer.prepare();
        } catch (IllegalStateException e) {
            Toast.makeText(getApplicationContext(), "Server is down!", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            Toast.makeText(getApplicationContext(), "Server is down!", Toast.LENGTH_LONG).show();
        }
        if(!mPlayer.isPlaying()) {
            mPlayer.start();
        }else{
            mPlayer.stop();
            mPlayer = new MediaPlayer();
            mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            try {
                mPlayer.setDataSource(s);
            } catch (IllegalArgumentException e) {
                Toast.makeText(getApplicationContext(), "Server is down", Toast.LENGTH_LONG).show();
            } catch (SecurityException e) {
                Toast.makeText(getApplicationContext(), "Server is down", Toast.LENGTH_LONG).show();
            } catch (IllegalStateException e) {
                Toast.makeText(getApplicationContext(), "Server is down", Toast.LENGTH_LONG).show();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                mPlayer.prepare();
            } catch (IllegalStateException e) {
                Toast.makeText(getApplicationContext(), "Server is down", Toast.LENGTH_LONG).show();
            } catch (IOException e) {
                Toast.makeText(getApplicationContext(), "Server is down", Toast.LENGTH_LONG).show();
            }
            mPlayer.start();
        }

    }
}